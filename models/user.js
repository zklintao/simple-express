const Sequelize = require("sequelize");
const db = require("./db");

// define user table
const User = db.define("user", {
  uuid: {
    type: Sequelize.UUID,
    primaryKey: true,
    defaultValue: Sequelize.UUIDV1
  },
  username: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: false,
    validate: {
      len: {
        args: [8, 64],
        msg: "must have at least 8 characters and maximum of 64 characters"
      },
      isUsername (value) {
        if (!/[a-zA-Z0-9][a-zA-Z0-9_]*/.test(value)) {
          throw new Error("can only contain letters, numbers and the underscore '_' (not the first letter)");
        }
      }
    }
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false
  },
  salt: {
    type: Sequelize.UUID,
    allowNull: false
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  age: {
    type: Sequelize.INTEGER,
    allowNull: false,
    validate: {
      isInt: {
        msg: "must be an integer number"
      }
    }
  },
  avatar: {
    type: Sequelize.STRING,
    allowNull: true
  }
}, {
  defaultScope: {
    attributes: {
      exclude: ["password", "salt", "updatedAt", "createdAt"]
    }
  }
});

// create user table if not exists
User.sync({
  force: false
});

module.exports = User;

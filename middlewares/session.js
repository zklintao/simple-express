const Redis = require("ioredis");
const sessionRedis = new Redis({
  keyPrefix: "session:"
});
const uuidv1 = require("uuid/v1");

class Session {
  static async start(req, res, next) {
    let session = new Session();
    req.session = session;
    session.token = req.cookies.token || uuidv1();
    res.cookie("token", session.token, {
      maxAge: 90000,
      httpOnly: true
    });
    let result = await sessionRedis.multi()
      .hgetall(session.token)
      .expire(session.token, 900)
      .exec();

    session.userInfo = result[0][1];
    next();
  };

  async set(key, value) {
    let result = await sessionRedis.multi()
      .hmset(this.token, {
        [key]: value
      })
      .expire(this.token, 900)
      .exec();
    return result[0][0];
  };
}

module.exports = Session;
"use strict";

const express = require("express");
const router = express.Router();
const Session = global.rootRequire("./middlewares/session");
const User = global.rootRequire("./models/user");
const Sequelize = require("sequelize");
const Promise = require("bluebird");
const fs = Promise.promisifyAll(require("fs"));
const path = require("path");
const multer = require("multer");
const uuidv1 = require("uuid/v1");
const uuidv4 = require("uuid/v4");
const isUUID = require("validator/lib/isUUID");
const crypto = require("crypto");

// register
router.post("/register", async(req, res, next) => {
  let body = req.body;

  // does user exist ?
  try {
    let user = await User.findOne({
      where: {
        username: body.username
      }
    });
    if (user) {
      let err = new Error("User Has Already Exist");
      err.status = 409;
      next(err);
      return;
    }
  } catch (err) {
    next(err);
    return;
  }

  // salted & hashed password
  let salt = uuidv4();
  let hmac = crypto.createHmac("sha1", salt);
  hmac.update(body.password);
  let hashedPassword = hmac.digest("hex");

  // create user
  User
    .create({
      username: body.username,
      password: hashedPassword,
      salt: salt,
      name: body.name,
      age: body.age
    })
    .then(user => {
      user = user.toJSON();
      delete user.password;
      delete user.salt;
      res.status(201).json(user);
    })
    .catch(err => {
      err.status = 400;
      next(err);
    });
});

// login
router.post("/login", async(req, res, next) => {
  let body = req.body;

  // does user exist ?
  let user;
  try {
    user = await User.findOne({
      where: {
        username: body.username
      },
      attributes: {
        exclude: ["updatedAt", "createdAt"]
      }
    });
    if (!user) {
      let err = new Error("User Does Not Exist");
      err.status = 404;
      next(err);
      return;
    }
  } catch (err) {
    next(err);
    return;
  }

  // salt password
  let salt = user.salt;
  let hmac = crypto.createHmac("sha1", salt);
  hmac.update(body.password);
  let hashedPassword = hmac.digest("hex");

  // check if password correct
  if (user.password === hashedPassword) {
    // loged in
    await req.session.set("uuid", user.uuid);
    res.status(200).end();
  } else {
    let err = new Error("Wrong Password");
    err.status = 400;
    next(err);
  }
});

// edit
router.put("/edit", async(req, res, next) => {
  let uuid = req.session.userInfo.uuid;

  let user;
  try {
    user = await User.findById(uuid);
    if (!user) {
      let err = new Error("User Does Not Exist");
      err.status = 404;
      next(err);
      return;
    }
  } catch (err) {
    next(err);
    return;
  }

  let body = req.body;

  // update if field exist
  if (body.name) {
    user.name = body.name;
  }
  if (body.age) {
    user.age = body.age;
  }

  // save updates
  user
    .save()
    .then(updatedUser => {
      res.json(updatedUser);
    })
    .catch(err => {
      err.status = 400;
      next(err);
    });
});

// file uploader
const storage = multer.diskStorage({
  destination: function (req, file, callback) {
    const uploadFolder = "./public/uploads/";
    const sep = path.sep;
    const initDir = path.isAbsolute(uploadFolder) ? sep : "";
    Promise
      .reduce(uploadFolder.split(sep).filter(x => x), (parentDir, childDir) => {
        const curDir = path.resolve(parentDir, childDir);
        return fs.statAsync(curDir)
          .catch(() => {
            return fs.mkdirAsync(curDir)
              .catch({
                code: "EEXIST"
              }, () => {});
          })
          .then(() => {
            return curDir;
          });
      }, initDir)
      .then(() => {
        callback(null, uploadFolder);
      })
      .catch(err => {
        callback(err);
      });
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + "_" +
      uuidv1().replace(/-/g, "") + path.extname(file.originalname));
  }
});
const upload = multer({
  storage: storage,
  fileFilter: (req, file, callback) => {
    callback(null, true);
  }
});

// upload an avatar
router.put("/avatar", upload.single("avatar"), async(req, res, next) => {
  let uuid = req.session.userInfo.uuid;

  let user;
  try {
    user = await User.findById(uuid);
    if (!user) {
      let err = new Error("User Does Not Exist");
      err.status = 404;
      next(err);
      return;
    }
  } catch (err) {
    next(err);
    return;
  }

  let file = req.file;

  if (file && file.path) {
    user.avatar = file.path;
  }

  user.save()
    .then(updatedUser => {
      res.json(user);
    })
    .catch(err => {
      err.status = 400;
      next(err);
    });
});

router.get("/avatars", function (req, res, next) {
  const uploadFolder = "./public/uploads/";
  fs.readdirAsync(uploadFolder)
    .reduce(function (totalSize, file) {
      return fs.statAsync(path.resolve(uploadFolder, file))
        .then((stat) => {
          return totalSize + stat.size;
        })
        .catch({
          code: "ENOENT"
        }, () => {});
    }, 0)
    .then(totalSize => {
      res.json({
        totalSize: totalSize
      });
    })
    .catch(err => {
      next(err);
    });
});

// catch 404 error
router.use(function (req, res, next) {
  let err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// catch validation error
router.use(function (err, req, res, next) {
  if (err instanceof Sequelize.ValidationError) {
    err.messages = err.errors.map(error => {
      return error.path + " " + error.message;
    });
  }
  next(err);
});

// error handler
router.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    error: err.messages || [err.message] || null
  });
});

module.exports = router;